package org.benignbala.groovy.requests

/**
 * Created by bala on 30/12/15.
 */
class HTTPRequest {
    URL url
    Map<String, String> headers
    Map<String, Object> params
    String httpVersion

    HTTPRequest(String reqUrl) {
        this.url = reqUrl.toURL()
        this.httpVersion = "1.1"
        this.headers = [:]
    }

    void setHeader(String header, String value) {
        this.headers[header.capitalize()] = value
    }

    HTTPResponse get(params = [:]) {
        String reqUrl = "GET " + this.url.toString() + " HTTP/${this.httpVersion}"
        String reqHeaders
        this.headers.each {h,v ->
            reqHeaders += "${h}: ${v}\r\n"
        }
        String request = "${reqUrl}\r\n"
        if (reqHeaders != null) {
            request += "${reqHeaders}"
        }

        request += "\r\n"
        def httpSock = new HTTPSocketInterface(this.url.getHost(), request)
        def rawHttpResponse = httpSock.transact()
        return new HTTPResponse(rawHttpResponse)
    }

}
