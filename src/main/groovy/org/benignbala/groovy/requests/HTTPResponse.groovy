package org.benignbala.groovy.requests

/**
 * Created by bala on 30/12/15.
 */
class HTTPResponse {
    Map<String, String> headers
    def status
    String statusLine
    String responseBody
    String responseContent
    Boolean inHeaders = false
    Boolean inStatusLine = true
    Boolean inContent = false
    def statusLinePattern = /^HTTP\/\d\.\d\s(\d+).*/
    def headerPattern = /^(\S+)\s*:\s*(.*)/

    HTTPResponse(List<Object> data) {
        responseBody = ""
        status = 200
        responseContent = ""
        data.each {
            responseBody += it
            if (inStatusLine) {
                def matched = (it =~ statusLinePattern)
                if (matched.groupCount() > 0) {
                    status = matched.group(0)
                    inStatusLine = false
                    inHeaders = true
                }
            }

            if (inHeaders) {
                def matched = (it =~ headerPattern)
                if (matched.hasGroup()) {
                    headers[matched[1][1]] = matched[1][2]
                } else {
                    inHeaders = false
                    inContent = true
                }
            }

            if (inContent) {
                responseContent += it
            }
        }
    }

}
