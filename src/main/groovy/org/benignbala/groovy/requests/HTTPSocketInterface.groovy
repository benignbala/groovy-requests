package org.benignbala.groovy.requests

/**
 * Created by bala on 30/12/15.
 */
class HTTPSocketInterface {
    String host
    Socket socket
    String data

    HTTPSocketInterface(String host, String data) {
        socket = new Socket(host, 80)
        this.data = data
    }

    List transact() {
        def response = []
        this.data.split("\r\n").each {
            it.trim()
            print "${it}\r\n"
            this.socket << "${it}\r\n"
        }
        print "\r\n"
        this.socket << "\r\n"

        this.socket.withStreams {input, output ->
            input.eachLine {
                println it
                response << it
                if (it.equals("\r\n")) {
                    return
                }
            }
        }
        return response
    }
}
