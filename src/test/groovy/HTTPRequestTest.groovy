import spock.lang.Specification
import org.benignbala.groovy.requests.*
/**
 * Created by bala on 30/12/15.
 */
class HTTPRequestTest extends Specification {

    def http

    void setup() {
        http = new HTTPRequest("http://t.co/C0iSl6AQ")
    }

    def "getTest"() {
        when:
        def resp = http.get()
        println resp.status

        then:
        0 != resp.status
    }

    void cleanup() {
        http = null
    }


}
